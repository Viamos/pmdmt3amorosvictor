package com.badlogicgames.plane;

/**
 * Created by damviod on 22/04/16.
 */

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
/**
 * Created by damviod on 22/04/16.
 */
public class MainMenuScreen implements Screen {
    final MainGame game;
    OrthographicCamera camera;
    Music music;

    public  MainMenuScreen(final MainGame _game){
        game = _game;
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);
        music = Gdx.audio.newMusic(Gdx.files.internal("silence.mp3"));
        music.setLooping(true);
        music.play();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        game.batch.setProjectionMatrix(camera.combined);

        game.batch.begin();
        game.font.draw(game.batch, "Benviguts al DAM PLANE DOWN!!! ", 100, 150);
        game.font.draw(game.batch, "TAP TO START!", 100, 100);
        game.batch.end();

        if(Gdx.input.isTouched()){
            game.setScreen(new PlaneGame(game));
            dispose();
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}