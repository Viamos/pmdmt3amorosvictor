package com.badlogicgames.plane;

/**
 * Created by damviod on 22/04/16.
 */
import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by damviod on 22/04/16.
 */
public class MainGame extends Game {

    SpriteBatch batch;
    BitmapFont font;

    public void create() {
        batch = new SpriteBatch();
        font = new BitmapFont();
        this.setScreen(new MainMenuScreen(this));
    }

    public void render(){super.render(); }

    public void dispose(){
        batch.dispose();
        font.dispose();
    }
}
