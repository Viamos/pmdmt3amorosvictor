package com.badlogicgames.plane;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by damviod on 22/04/16.
 */
public class Plane {

    Animation anim;
    Vector2 planePosition = new Vector2();
    Vector2 planeVelocity = new Vector2();
    float planeStateTime = 0;

    public Plane() {

        Texture frame1 = new Texture("plane1.png");
        frame1.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        Texture frame2 = new Texture("plane2.png");
        Texture frame3 = new Texture("plane3.png");

        anim = new Animation(0.05f, new TextureRegion(frame1), new TextureRegion(frame2), new TextureRegion(frame3), new TextureRegion(frame2));
        anim.setPlayMode(Animation.PlayMode.LOOP);

    }

    public void render(){

    }

    public void dispose(){

    }
}
